![columbusegg4delphi logo](https://bytebucket.org/bittimepro/columbusegg4delphi/raw/0f07f25211020770bfc877e35f1b23d8c80caee3/resources/ColumbusEgg4DelphiLogo.png)

# ColumbusEgg4Delphi is...

- A simple to use Delphi framework to correctly write business logic in Delphi "RAD style" applications. 
- Based on Table Module design pattern but adapted to be Delphi friendly and dataset centric.
- Provides all the good alternatives to do the common Delphi practices which makes your application hard to test, in a testable way.
- Provides observers and listeners to avoid all the business logic code in your form.
- Retain almost all the good of Delphi RAD.
- Developed by bit Time Professionals.

Check the Official Embarcadero Webinar where Daniele Teti introduced columbusegg4delphi: [MVVM-MVC-RAD Architectures with ColumbusEgg4Delphi](https://community.embarcadero.com/blogs/entry/mvvm-mvc-rad-architectures-with-columbusegg4delphi)


# Why ColumbusEgg4Delphi?
- Forms and datamodules are free of Business Logic code.
- Provides a single point for the BL code
- You can do unit tests; Business Logic can be unit tested using in-memory datasets.
- You can still use the RAD approach to design you forms
- If your data comes from an application server, you can fill in-memory datasets (or virtual dataset) and still use the same Business Logic.

## Very Important!
- Columbusegg4delphi can be used also in legacy applications to improve their architecture at very low cost.
  - Just create a columbus module for each dataset and move there all the events related code. It is an incremental approach; you can change it one dataset at time.


## Integrate columbusegg4delphi into existing projects 
During the start of columbus (for short) integration you could face different kind of problems related to utilization of datasets features. Here's a list of common pattern that you could face.

## Accessing "DataSet2" inside "DataSet1" event handlers

Usually, in the following events you may need to access other datasets:
* OnCalcFields
* OnNewRecord
* AfterPost

For instance: 
```delphi
procedure TdmUtenti.dsUtentiGruppiNewRecord(DataSet: TDataSet);
begin
  dsUtentiGruppiID_UTENTE.AsInteger := dsUtenteID.AsInteger;
end;
```

The solution is to use the ```Modules``` property which contains all the columbus modules created on the same design "surface" (as form or datamodule or webmodule).

```delphi
procedure TUtentiGruppiColumbus.OnNewRecord(aDataSet: TDataSet);
var
  UtenteColumbus: TUtenteColumbus;
begin
  inherited;
  UtenteColumbus := modules['TUtenteColumbus'] as TUtenteColumbus;

  DataSet.FieldByName('ID_UTENTE').AsInteger := UtenteColumbus.DataSet.FieldByName('ID').AsInteger;
end;
```

Every time we create a **ColumbusModule**, the framework saves in a dictionary  all the modules created.

So in the previoous example, the datamodule *"TdmUtenti"* will creates 2 columbus modules in the DataModuleCreate event handler: *"TUtenteColumbus"* and *"TUtentiGruppiColumbus"*.

```delphi
procedure TdmUtenti.DataModuleCreate(Sender: TObject);
begin
  FUtenteGruppiColumbus := TUtentiGruppiColumbus.Create(dsUteGruppi);
  FUtenteColumbus := TUtenteColumbus.Create(ds);
end;
```

## Messages Handling ##
-----------
How to use IColumbusUIListener...

## Specific Dataset descendant methods not available in TDataSet class (e.g. CachedUpdates) ##
-----------

How to use IColumbusService...