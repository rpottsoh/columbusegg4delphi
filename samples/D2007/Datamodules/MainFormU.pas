unit MainFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, DB, ExtCtrls, DBCtrls,
  Grids, DBGrids,
  ColumbusModuleCustomersU, StdCtrls,
  ColumbusCommons, MainDataModuleU,
  ActnList, ImgList, Buttons;

type
  TMainForm = class(TForm, IColumbusObserver)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    Panel1: TPanel;
    Label1: TLabel;
    DataSource2: TDataSource;
    DBGrid2: TDBGrid;
    Panel2: TPanel;
    Label2: TLabel;
    SaveDialog1: TSaveDialog;
    Panel3: TPanel;
    btnExport: TSpeedButton;
    Button1: TSpeedButton;
    ActionList1: TActionList;
    actExportCustomers: TAction;
    actGeocoding: TAction;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure actExportCustomersExecute(Sender: TObject);
    procedure actGeocodingExecute(Sender: TObject);
  public
    procedure UpdateObserver(const Sender: TObject; const ModuleName: String);
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}


uses ColumbusModuleSalesU;

procedure TMainForm.actExportCustomersExecute(Sender: TObject);
begin
  if SaveDialog1.Execute(self.Handle) then
  begin
    // you can also call a specialized method on the datamodule which call the ExportToFile
    DataModuleMain.ExportToFile(SaveDialog1.FileName);
  end;

end;

procedure TMainForm.actGeocodingExecute(Sender: TObject);
begin
  DataModuleMain.CustomerGeocoding;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  DataModuleMain.CustomerModule.RegisterObserver(self);
  DataModuleMain.SalesModule.RegisterObserver(self);
  DataModuleMain.dsSales.Open;
  DataModuleMain.dsCustomers.Open;
end;

procedure TMainForm.UpdateObserver(const Sender: TObject; const ModuleName: String);
var
  lCustomerModule: TCustomerModule;
begin
  if SameText(ModuleName, 'TCustomerModule') then
  begin
    lCustomerModule := Sender as TCustomerModule;
    Label1.Caption := Format('%d persons lives in California',
      [lCustomerModule.PeopleInCalifornia]) + sLineBreak +
      'Sales count: ' + IntToStr(lCustomerModule.SalesCount);
    Label3.Visible := lCustomerModule.GeocodeIsValid;
    if Label3.Visible then
    begin
      Label3.Caption := Format('Lat: %.6f' + sLineBreak + 'Lon: %.6f',
        [lCustomerModule.CustomertLat, lCustomerModule.CustomerLon]);
    end;
  end;
  if SameText(ModuleName, 'TSalesModule') then
  begin
    Label2.Caption := 'TOTAL SALES VALUE: � ' +
      FormatCurr('0.00', (Sender as TSalesModule).TotalValue);
  end;
end;

end.
