unit TestColumbusModule_DU;

interface

uses
  DUnitX.TestFramework, Data.DB, ColumbusModule.CustomersU,
  FireDAC.Comp.DataSet,
  ColumbusCommons, FireDAC.Comp.Client;

type

  [TestFixture]
  TColumbusPrimerTestInsight = class(TObject)
  private
    FDataSet: TDataSet;
    FCustomerModule: TCustomerModule;
    function GetCustomersDataset: TDataSet;
    procedure PopulateDataSet;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestCalcCaliforniaPersons;
    [Test]
    procedure TestIsItalianCustomer;
    [Test]
    procedure TestDefaultForNewCustomer;
    [Test]
    procedure TestMandatoryFields;
    [Test]
    procedure TestNotify;
    [Test]
    procedure TestCannotDeleteItalianPeople;

  end;

implementation

uses
  System.SysUtils;

function TColumbusPrimerTestInsight.GetCustomersDataset: TDataSet;
var
  lMemTable: TFDMemTable;
begin
  lMemTable := TFDMemTable.Create(nil);
  lMemTable.FieldDefs.Add('CONTACT_FIRST', ftString, 100);
  lMemTable.FieldDefs.Add('CONTACT_LAST', ftString, 100);
  lMemTable.FieldDefs.Add('STATE_PROVINCE', ftString, 100);
  lMemTable.FieldDefs.Add('COUNTRY', ftString, 100);
  Result := lMemTable;
end;

procedure TColumbusPrimerTestInsight.PopulateDataSet;
begin
  FDataSet.InsertRecord(['Bruce', 'Banner', 'NY', 'USA']);
  FDataSet.InsertRecord(['Peter', 'Parker', 'CA', 'USA']);
  FDataSet.InsertRecord(['Sue', 'Storm', 'CA', 'USA']);
  FDataSet.InsertRecord(['salvatore', 'sparacino', 'IT', 'Italy']);

end;

procedure TColumbusPrimerTestInsight.Setup;
begin
  FDataSet := GetCustomersDataset;
  FCustomerModule := TCustomerModule.Create(FDataSet);
end;

procedure TColumbusPrimerTestInsight.TearDown;
begin
end;

procedure TColumbusPrimerTestInsight.TestCalcCaliforniaPersons;
begin
  FDataSet.Open;
  PopulateDataSet;
  Assert.AreEqual(2, FCustomerModule.PeopleInCalifornia);
  FDataSet.Last;
  Assert.AreEqual(2, FCustomerModule.PeopleInCalifornia);
  FDataSet.InsertRecord(['Prova', 'Test', 'CA', 'USA']);
  Assert.AreEqual(3, FCustomerModule.PeopleInCalifornia);
  FDataSet.Delete;
  Assert.AreEqual(2, FCustomerModule.PeopleInCalifornia);
end;

procedure TColumbusPrimerTestInsight.TestCannotDeleteItalianPeople;
begin
  FDataSet.Open;
  PopulateDataSet;
  Assert.IsTrue(FDataSet.Locate('COUNTRY', 'Italy', []),
    'Cannot find italian people to test');

  Assert.WillRaiseWithMessage(
    procedure
    begin
      FDataSet.Delete;
    end, EColumbusException, 'You cannot delete italian customers!');

end;

procedure TColumbusPrimerTestInsight.TestDefaultForNewCustomer;
begin
  FDataSet.Open;
  FDataSet.Insert;
  Assert.AreEqual('IT', FDataSet.FieldByName('STATE_PROVINCE').AsString);
  Assert.AreEqual('Italy', FDataSet.FieldByName('COUNTRY').AsString);
end;

procedure TColumbusPrimerTestInsight.TestIsItalianCustomer;
begin
  FDataSet.Open;
  PopulateDataSet;
  Assert.IsTrue(FDataSet.Locate('COUNTRY', 'USA', []),
    'Cannot find USA people to test');
  Assert.IsFalse(FCustomerModule.IsItalianCustomer);

  Assert.IsTrue(FDataSet.Locate('COUNTRY', 'Italy', []),
    'Cannot find Italian people to test');
  Assert.IsTrue(FCustomerModule.IsItalianCustomer);

end;

procedure TColumbusPrimerTestInsight.TestMandatoryFields;
begin
  FDataSet.Open;
  FDataSet.Insert;

  Assert.WillRaiseWithMessage(
    procedure
    begin
      FDataSet.Post;
    end, EColumbusException, 'First name and last name are mandatory');

end;

procedure TColumbusPrimerTestInsight.TestNotify;
var
  lCalled: Boolean;
begin
  lCalled := False;
  FCustomerModule.RegisterObserver(TColumbusMockObserver.Create(
    procedure(Sender: TObject; ModuleName: String)
    begin
      lCalled := true;
    end));

  FDataSet.Open;
  Assert.IsTrue(lCalled);

  lCalled := False;
  // no notify if scroll
  FDataSet.Next;
  Assert.IsFalse(lCalled);

  FDataSet.InsertRecord(['Scott', 'Summers', 'CA', 'USA']);
  Assert.IsTrue(lCalled);
end;

initialization

TDUnitX.RegisterTestFixture(TColumbusPrimerTestInsight);

end.
